<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public function searchMovies() {
        return view('home');
    }
    public function findMovie(Request $request) {
        $client = new Client();
        $api_request = $client->request('POST', 'http://www.omdbapi.com', [
            'query' => [
                't' => $request->title,
                'apikey' => '88ff0293'
            ]
        ]);
        $bodyJSON = $api_request->getBody();
        $body = json_decode($bodyJSON);
        if ($body->Response == 'False') {
            return redirect(route('home'))->with(['message' => 'Movie not found!']);
        } else {
            return view('show', compact('body'));
        }
    }

    public function addMovie(Request $request) {
        $title = $request->title;
        $poster = $request->poster;
        $genre = $request->genre;
        $plot = $request->plot;
        $director = $request->director;
        $writer = $request->writer;
        $actors = $request->actors;
        $rating = $request->rating;
        $votes = $request->votes;
        $runtime = $request->runtime;
        $movie = new Movie();
        $movie->title = $title;
        $movie->poster = $poster;
        $movie->genre = $genre;
        $movie->plot = $plot;
        $movie->director = $director;
        $movie->writer = $writer;
        $movie->actors = $actors;
        $movie->rating = $rating;
        $movie->votes = $votes;
        $movie->runtime = $runtime;
        $movie->save();
        return redirect()->route('movies')->with(['message' => 'Movie Added Succesfully!']);
    }

    public function editMovie($id) {
        $movie = Movie::findOrFail($id);
        return view('edit', compact('movie'));
    }

    public function editSuccess(Request $request, $id) {
        $movie = Movie::findOrFail($id);
        $movie->poster = $request->poster;
        $movie->genre = $request->genre;
        $movie->plot = $request->plot;
        $movie->director = $request->director;
        $movie->writer = $request->writer;
        $movie->actors = $request->actors;
        $movie->rating = $request->rating;
        $movie->votes = $request->votes;
        $movie->runtime = $request->runtime;
        $movie->save();
        return redirect()->route('movies')->with(['message' => 'Movie Edited Succesfully!']);
    }

    public function deleteMovie($id) {
        $movie = Movie::destroy($id);
        return redirect()->route('movies')->with(['message' => 'Movie Deleted Succesfully!']);
    }

    public function allMovies(Request $request) {
        $limit = 5;
        $movies = Movie::where('id' ,'>', '0')->orderBy('created_at', 'DESC')->skip($request->offset)->limit($limit)->get();
        if ($request->ajax()) {
            return response()->json(['movies' => $movies]);
        }
        return view('movies', compact('movies'));
    }
    public function showMore() {

    }
}
