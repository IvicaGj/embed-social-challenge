<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'SearchController@searchMovies')->name('home');
Route::post('/find', 'SearchController@findMovie')->name('find');
Route::post('/add/movie', 'SearchController@addMovie')->name('add.movie');
Route::get('/edit/movie/{id}', 'SearchController@editMovie')->name('edit.movie');
Route::post('/edit/success/{id}', 'SearchController@editSuccess')->name('edit.success');
Route::get('/delete/movie/{id}', 'SearchController@deleteMovie')->name('delete.movie');
Route::get('/movies', 'SearchController@allMovies')->name('movies');
Route::get('/show/more/movies', 'SearchController@allMovies')->name('show.more.movies');
