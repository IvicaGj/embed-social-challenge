@extends('layout')

@section('content')
    <h1 class="h1 mt-5">Find A Movie</h1>
    <form class="form-group" action="{{ route('find') }}" method="POST">
        <input type="text" class="form-control" name="title" placeholder="e.g. Harry Potter">
        <button type="submit" class="btn btn-primary float-right mt-3">Search</button>
        @csrf
    </form>
    <h2 class="mt-4">Search Results</h2>
    <div class="col-12">
        <table class="table table-hover">
            <tbody>
            <tr>
                <td>
                    <img src="{{ $body->Poster }}"/>
                </td>
                <td>
                    <strong>{{ $body->Title }}({{$body->Year}})</strong><br/>
                    {{ $body->imdbRating }}/10<br/>
                    @if($body->Rated == 'N/A')
                        {{ 'unrated' }}
                    @else
                        {{ $body->Rated }}
                    @endif
                    {{ ' | ' }}
                    @if($body->Runtime == 'N/A')
                        {{ 'unknown' }}
                    @else
                        {{ $body->Runtime }}
                    @endif
                    <br/>
                    {{ $body->Genre }} | {{ $body->Released }}
                </td>
                <td>
                    {{ $body->Plot }}
                    <br/>
                    <br/>
                    Director: {{$body->Director}}
                    <br/>
                    <br/>
                    Writer: {{ $body->Writer }}
                    <br/>
                    <br/>
                    Actors: {{ $body->Actors }}
                </td>
                <td>
                    <form action="{{ route('add.movie') }}" method="POST">
                        <input type="hidden" name="poster" value="{{ $body->Poster }}">
                        <input type="hidden" name="title" value="{{ $body->Title }}">
                        <input type="hidden" name="genre" value="{{ $body->Genre }}">
                        <input type="hidden" name="plot" value="{{ $body->Plot }}">
                        <input type="hidden" name="director" value="{{ $body->Director }}">
                        <input type="hidden" name="writer" value="{{ $body->Writer }}">
                        <input type="hidden" name="actors" value="{{ $body->Actors }}">
                        <input type="hidden" name="rating" value="{{ $body->imdbRating }}">
                        <input type="hidden" name="votes" value="{{ $body->imdbVotes }}">
                        <input type="hidden" name="runtime" value="{{ $body->Runtime }}">
                        <button class="btn btn-success" type="submit"><i class="fas fa-plus"></i></button>
                        @csrf
                    </form>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Toggle Menu</a>
@endsection
