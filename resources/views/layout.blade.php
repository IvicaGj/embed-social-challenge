<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Find Movies</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="{{ route('home') }}">Find Movies</a>
                </li>
                <li>
                    <a href="{{ route('home') }}">Add Movie</a>
                </li>
                <li>
                    <a href="{{ route('movies') }}">All Movies</a>
                </li
            </ul>
        </div>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    @if(session()->has('message'))
        <script>
            $(document).ready(function() {
               alert('{{session()->get('message')}}');
            });
        </script>
    @endif
    <script>
        $(document).ready(function() {
            let offset = 5;
            $("#loadmore").click(function(e) {
                e.preventDefault();
                offset += 5;
                loadmore(offset);
            });
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        });
        function loadmore(offset) {
            $.ajax({
                url: '/show/more/movies?offset=' + offset,
                method: "GET",
                data: JSON,
                success: function success(data) {
                    var movies = data['movies']
                    for ($i = 0; $i < 5; $i++ ) {
                        $('#loadcontent').append(
                           "<tr>" +
                                "<td><img src=\"" + movies[$i]['poster'] + "\"></td>\n" +
                                "<td>" + movies[$i]['title'] + "</td>\n" +
                                "<td>" + movies[$i]['genre'] + "</td>\n" +
                                "<td>" + movies[$i]['plot'] + "</td>\n" +
                                "<td>" + movies[$i]['director'] + "</td>\n" +
                                "<td>" + movies[$i]['writer'] + "</td>\n" +
                                "<td>" + movies[$i]['actors'] + "</td>\n" +
                                "<td>" + movies[$i]['rating'] + "</td>\n" +
                                "<td>" + movies[$i]['votes'] + "</td>\n" +
                                "<td>" + movies[$i]['runtime'] + "</td>\n" +
                                "<td>\n" +
                                    "<a href=\"/edit/movie/" + movies[$i]['id'] + "\"><button class=\"btn btn-dark mt-3\"><i class=\"fas fa-pencil-alt\"></i></button></a>\n" +
                                    "<a href=\"/delete/movie/" + movies[$i]['id'] + "\"<button class=\"btn btn-dark mt-3\"><i class=\"fas fa-minus\"></i></button></td>\n" +
                                "</td>" +
                            "</tr>"
                        );
                        if (movies.length < 5 || movies.length === 0) {
                            $('#loadmore').hide();
                        }
                    };
                },
                error: function error(error) {}
            });
        }
    </script>
</body>
</html>
