@extends('layout')

@section('content')
    <div class="col-8">
        <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Toggle Menu</a>
        <h3 class="h1 mt-5 text-center">Edit {{ $movie->title }}</h3>
        <form class="form-group mt-5" action="{{ url('/edit/success').'/'.$movie->id }}" method="POST">
            <label>Poster URL</label>
            <input type="text" class="form-control" name="poster" value="{{ $movie->poster }}">
            <label>Genre</label>
            <input type="text" class="form-control" name="genre" value="{{ $movie->genre }}">
            <label>Plot</label>
            <textarea class="form-control" name="plot">{{ $movie->plot }}</textarea>
            <label>Director</label>
            <input type="text" class="form-control" name="director" value="{{ $movie->director }}">
            <label>Writer</label>
            <input type="text" class="form-control" name="writer" value="{{ $movie->writer }}">
            <label>Actors</label>
            <input type="text" class="form-control" name="actors" value="{{ $movie->actors }}">
            <label>Rating</label>
            <input type="text" class="form-control" name="rating" value="{{ $movie->rating }}">
            <label>Votes</label>
            <input type="text" class="form-control" name="votes" value="{{ $movie->votes }}">
            <label>Runtime</label>
            <input type="text" class="form-control" name="runtime" value="{{ $movie->runtime }}">
            <button type="submit" class="btn btn-primary float-right mt-3 mb-3">Edit</button>
            @csrf
        </form>
    </div>

@endsection
