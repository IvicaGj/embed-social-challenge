@extends('layout')

@section('content')
    <h1 class="h1 mt-5">Find A Movie</h1>
    <form class="form-group" action="{{ route('find') }}" method="POST">
        <input type="text" class="form-control" name="title" placeholder="e.g. Harry Potter">
        <button type="submit" class="btn btn-primary float-right mt-3">Search</button>
        @csrf
    </form>
    <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Toggle Menu</a>
@endsection
