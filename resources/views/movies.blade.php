@extends('layout')

@section('content')
    <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Toggle Menu</a>
    <h2 class="mt-4 mb-4">All Movies</h2>
    <div class="col-12">
        <table class="table table-hover movies">
            <thead class="text-uppercase font-weight-bold">
                <tr class="text-center">
                    <td>Poster</td>
                    <td>Title</td>
                    <td>Genre</td>
                    <td>Plot</td>
                    <td>Director</td>
                    <td>Writer</td>
                    <td>Actors</td>
                    <td>Rating</td>
                    <td>Votes</td>
                    <td>Runtime</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody id="loadcontent">
                @foreach($movies as $m)
                    <tr>
                        <td><img src="{{ $m->poster }}"></td>
                        <td>{{ $m->title }}</td>
                        <td>{{ $m->genre }}</td>
                        <td>{{ $m->plot }}</td>
                        <td>{{ $m->director }}</td>
                        <td>{{ $m->writer }}</td>
                        <td>{{ $m->actors }}</td>
                        <td>{{ $m->rating }}</td>
                        <td>{{ $m->votes }}</td>
                        <td>{{ $m->runtime }}</td>
                        <td>
                            <a href="{{ url('/edit/movie').'/'.$m->id }}"><button class="btn btn-dark mt-3"><i class="fas fa-pencil-alt"></i></button></a>
                            <a href="{{ url('/delete/movie').'/'.$m->id }}"<button class="btn btn-dark mt-3"><i class="fas fa-minus"></i></button></td>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button id="loadmore" class="btn btn-success text-center">Load More</button>
    </div>
@endsection
