<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->string('poster')->nullable();
            $table->text('plot')->nullable();
            $table->text('genre')->nullable();
            $table->text('director')->nullable();
            $table->text('writer')->nullable();
            $table->text('actors')->nullable();
            $table->string('rating')->nullable();
            $table->string('votes')->nullable();
            $table->string('runtime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
